import BookList from "./components/BookList"
import BookCreate from "./components/BookCreate"
import { useState } from "react"


export default function App() {
    const [books, setBooks] = useState([]);

    // NOTE: เรียกไอดีเพื่อทำการเปลี่ยนแปลงค่าด้านใน ที่เป็น "" และ return ค่าbook ใหม่กลับไป
    const editBookById = (id, newTitle) => {
        const updatedBooks = books.map((book) => {
            if (book.id === id) {
                return { ...book, title: newTitle }
            }
            return book
        })
        setBooks(updatedBooks)
    }

    const deleteBookById = (id) => {
        const updatedBooks = books.filter((book) => {
            return book.id !== id
        })
        setBooks(updatedBooks)
    }

    const createBook = (title) => {
        const updateBooks = [...books, { id: Math.round(Math.random() * 99999), title }]
        setBooks(updateBooks)
    }

    return (
        <div className="app">
            <h1>Reading List</h1>
            <BookList onEdit={editBookById} books={books} onDelete={deleteBookById} />
            <BookCreate onCreate={createBook} ></BookCreate>
        </div>
    )
}