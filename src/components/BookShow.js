import { useState } from "react";
import BookEdit from "./BookEdit";

export default function BookShow({ book, onDelete, onEdit }) {
    const [showEdit, setShowEdit] = useState(false)

    const handleDeleteClick = () => {
        onDelete(book.id)
    }
    // NOTE: Edit(1) ถ้าไม่click ก็ไม่แสดงการแก้ไข
    const handleEditClick = () => {
        setShowEdit(!showEdit)
    }

    // NOTE: Edit(3) อัพเดทรายการ การแก้ไข  และเมื่อกดปุ่ม save จะอัพเดทรายการใหม่ล่าสุดและปิดform นั้น
    const handleSubmit = (id, newTitle) => {
        setShowEdit(false)
        onEdit(id, newTitle)
    }
    // NOTE: Edit(2) *ต้อไว้ด้านล่างของปิดเสมอเพราะ ไม่ใช่ฟังก์เรียกการทำงาน* แสดงชื่อ และ ส่วนของการแก้ไข เมื่อกดปุ่ม
    let content = <h3>{book.title}</h3>
    if (showEdit) {
        content = <BookEdit onSubmit={handleSubmit} book={book} />
    }


    return (
        <div className="book-show">
            {/* เรียกรูปภาพมาโชว์ */}
            <img alt="books" src={`https://picsum.photos/seed/${book.id}/300/200`} />
            {/* แสดงส่วนของการเรียกแก้ไข */}
            <div>{content}</div>
            <div className="actions">
                <button onClick={handleEditClick} className="edit">Edit</button>
                <button onClick={handleDeleteClick} >Delete</button>
            </div>
        </div>);
}