import BookShow from "./BookShow";

export default function BookList({ books, onDelete, onEdit }) {

    // NOTE: showlist รายการหนังสือทั้งหมดที่เราพิมพ์ชื่อลงไป โดยที่โชว์จะมีปุ่มกดด้วย
    const renderedBooks = books.map((book) => {
        return <BookShow onEdit={onEdit} onDelete={onDelete} key={book.id} book={book} />
    })

    return (<div className="book-list">
        {renderedBooks}
    </div>);
}