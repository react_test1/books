import { useState } from "react"

export default function BookEdit({ book, onSubmit }) {
    // NOTE: สง Book มาและแสดงชื่อใน state นั้นๆ
    const [title, setTitle] = useState(book.title)


    // NOTE: event สำหรับการแก้ไข
    const handleChange = (event) => {
        setTitle(event.target.value)
    }

    // NOTE: ถ้าไม่มี event หรือการคลิ๊ก ไม่สามารถเปิด ฟอร์มแก้ไขนี้ได้
    const handleSubmit = (event) => {
        event.preventDefault();

        // NOTE: แทนข้อมูลไปเข้าไปใหม่ จากไอดีเดิม และ เรียกปิด form เมือทำการแก้ไข และกดปุ่ม save
        onSubmit(book.id, title)
    }

    return (
        <form onSubmit={handleSubmit} className="book-edit">
            <label>Title</label>
            <input className="input" value={title} onChange={handleChange} />
            <button>Save</button>
        </form>
    )
}